ARCH ?= arm
CROSS_COMPILE ?= arm-linux-gnueabihf-
KVER  ?= 5.15.92
KDIR ?= /home/user/linux-roadrunner/linux-5.15-mchp/
MODDESTDIR := /home/user/temp/mod
INSTALL_PREFIX := /
obj-m += samgpio.o

PWD := $(shell pwd)

default:
	$(MAKE) -C $(KDIR) SUBDIRS=$(PWD) modules
clean:
	$(MAKE) -C $(KDIR) SUBDIRS=$(PWD) clean

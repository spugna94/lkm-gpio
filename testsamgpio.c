/**
 * Just slightly modified from Derek Molloy's code
 * see http://www.derekmolloy.ie/ for a full description and follow-up descriptions.
*/
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<fcntl.h>
#include<string.h>

#define BUFFER_LENGTH 256
static char receive[BUFFER_LENGTH];

int main(){
   int ret, fd;
   char stringToSend[BUFFER_LENGTH];
   sprintf(stringToSend, "11100011001010");
   printf("Starting device test code example...\n");
   fd = open("/dev/pd2627", O_RDWR);             // Open the device with read/write access
   if (fd < 0){
      perror("Failed to open the device...");
      return errno;
   }
   
   while(1){
      ret = write(fd, stringToSend, strlen(stringToSend)); //output to PD27
      if (ret < 0) {
         perror("Failed to write the message to the device.");
         return errno;
      }
      
      ret = read(fd, receive, BUFFER_LENGTH);        // Read the response from the LKM
      if (ret < 0) {
         perror("Failed to read the message from the device.");
         return errno;
      }
      if (ret==0) { printf("."); fflush(stdout); }
      else printf("\n%d[%s]\n", ret, receive);
      sleep(1);
   }
   return 0;
}
